import { Component } from 'react';

class HelloWord extends Component {

  render() {
    return <h1>Hello, World!</h1>
  }
}

export default HelloWord
